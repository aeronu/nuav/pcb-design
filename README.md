# PCB Design

This repo is dedicated to holding all the pcb files that are designed by NUAV. The primary CAD software that we will use is [Altium](https://www.altium.com/altium-trial-flow). You should be able to get a free subscription by using your school email.

To learn the basics or just to get familiar with Altium, follow this set of [Altium tutorials](https://www.youtube.com/watch?v=KpgTud1iQ-4&list=PLXvLToQzgzdfKKQn2wmpuSXz6sROQmO6R).

If you are unfamiliar with git, follow this [git tutorial](https://www.youtube.com/watch?v=FsV_OWtB2EA) to learn the basics.

